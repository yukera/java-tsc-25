package com.tsc.jarinchekhina.tm.api.entity;

import com.tsc.jarinchekhina.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
