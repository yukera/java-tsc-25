package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "display developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        @NotNull final String developer = serviceLocator.getPropertyService().getDeveloper();
        System.out.println("Developer: " + developer);
        @NotNull final String email = serviceLocator.getPropertyService().getDeveloperEmail();
        System.out.println("E-mail: " + email);
    }

}
