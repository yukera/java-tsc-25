package com.tsc.jarinchekhina.tm.command.system;

import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "display program version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        @NotNull final String version = serviceLocator.getPropertyService().getVersion();
        System.out.println(version);
    }

}
